= pacapt(1)
:doctype: manpage

== Name

pacapt - An Arch's pacman-like package manager for some Unices.

== Synopsis

*pacapt* _[option(s)]_ _[operation(s)]_ _[package(s)]_

== Operations

*-h*, *--help*::
    print this help message

*-P*::
    print supported operations

*-V*::
    print version information

*-Q*::
    list all installed packages

*-Qc* _package_::
    show package's changelog

*-Qi* _package_::
    print package status

*-Ql* _package_::
    list package's files

*-Qm*::
    list installed packages that aren't available in any installation source

*-Qo* _file_::
    query package that provides _<file>_

*-Qp* _file_::
    query a package file (don't use package database)

*-Qs* _package_::
    search for installed pack

*-S* _package_::
    install package(s)

*-Ss* *package*::
    search for packages

*-Su*::
    upgrade the system

*-Sy*::
    update package database

*-Suy*::
    update package database, then upgrade the system

*-R* _packages_::
    remove some packages

*-Sc*::
    delete old downloaded packages

*-Scc*::
    delete all downloaded packages

*-Sccc*::
    clean variant files. (debian) See also http://dragula.viettug.org/blogs/646

== Options

*-w*::
    download packages but don't install them

*--noconfirm*::
    don't wait for user's confirmation

== Examples

To install a package from Debian's backports repository

----
$ pacapt -S foobar -t lenny-backports
$ pacapt -S -- -t lenny-backports foobar
----

To update package database and then update your system

----
$ pacapt -Syu
----

To download a package without installing it

----
$ pacapt -Sw foobar
----

== Author

This manual page was written by mailto:czchen@debian.org[ChangZhuo Chen
'<czchen@debian.org>'] for the *Debian GNU/Linux system* (but may be used by
others).
